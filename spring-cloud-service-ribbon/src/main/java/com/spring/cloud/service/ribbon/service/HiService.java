package com.spring.cloud.service.ribbon.service;

public interface HiService {
    String hiService(String name);
}
