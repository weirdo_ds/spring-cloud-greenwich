package com.spring.cloud.service.ribbon.service.impl;

import com.spring.cloud.service.ribbon.service.HiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class HiserviceImpl implements HiService {

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public String hiService(String name) {
        return restTemplate.getForObject("http://service-hi/hi?name="+name , String.class );
    }
}
