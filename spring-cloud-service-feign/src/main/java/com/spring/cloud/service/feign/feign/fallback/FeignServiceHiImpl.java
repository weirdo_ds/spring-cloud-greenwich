package com.spring.cloud.service.feign.feign.fallback;

import com.spring.cloud.service.feign.feign.FeignServiceHi;
import org.springframework.stereotype.Component;

@Component
public class FeignServiceHiImpl implements FeignServiceHi {

    @Override
    public String sayHiFromClientOne(String name) {
        return "sorry, you are fail,"+name;
    }
}
