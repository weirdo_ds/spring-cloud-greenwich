package com.spring.cloud.service.feign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class GServiceFeignApplication {
    public static void main(String[] args) {
        SpringApplication.run( GServiceFeignApplication.class , args );
        System.out.println("finsh.");
    }
}
