package com.spring.cloud.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class GEurekaApplication {
    public static void main(String[] args) {
        SpringApplication.run(GEurekaApplication.class , args  );
        System.out.println("finsh.");
    }
}
